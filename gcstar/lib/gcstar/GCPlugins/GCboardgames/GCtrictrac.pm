package GCPlugins::GCboardgames::GCtrictrac;

###################################################
#
#  Copyright 2005-2016 Christian Jodar
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;

use GCPlugins::GCboardgames::GCboardgamesCommon;

{
    package GCPlugins::GCboardgames::GCPlugintrictrac;

    use base qw(GCPlugins::GCboardgames::GCboardgamesPluginsBase);
 
    sub start
    {
        my ($self, $tagname, $attr, $attrseq, $origtext) = @_;
	
        $self->{inside}->{$tagname}++;

        if ($self->{parsingEnded})
        {
            return;
        }
        

        if ($self->{parsingList})
        {
            # Parse the search results here

            # Check if we are currently parsing an item page, not a search results page (ie - exact match has taken us straight to the page)
            # Do this by checking if there is a heading on the page
            if (($tagname eq "font") && ($attr->{style} =~ /FONT-SIZE: 20px/))
            {
                # Stop parsing results, switch to item parsing
                $self->{parsingEnded} = 1;
                $self->{itemIdx} = 0;
                $self->{itemsList}[0]->{url} = $self->{loadedUrl};
            }

            if (($tagname eq "div") && ($attr->{class} eq "title"))
            {
                $self->{isBoardgame} = 1;
            }
            elsif (($tagname eq "a") && ($self->{isBoardgame} eq 1))
            {
                # Add to search results
                $self->{itemIdx}++;
                $self->{itemsList}[$self->{itemIdx}]->{url} =  $attr->{href};
                $self->{itemsList}[$self->{itemIdx}]->{name} = $attr->{title};
            }
            elsif (($tagname eq "span") && ($attr->{class} eq "released") && ($self->{isBoardgame} eq 1))
            {
                $self->{isDate} = 1;
            }
            elsif (($tagname eq "div") && ($self->{isBoardgame} eq 1))
            {
                $self->{isBoardgame} = 0;
            }
        }
        else
        {
            # Parse the items page here. Basically we do this by seaching for tags which match certain criteria, then preparing to grab
            # the text inside these tags

            if (($tagname eq "img") && ($attr->{id} eq "img-game"))
            {
                $self->{curInfo}->{name} = $attr->{alt};
                $self->{curInfo}->{boxpic} = $attr->{src};
                $self->{curInfo}->{boxpic} =~ s/https/http/g;
            }
            elsif (($tagname eq "a") && ($attr->{class} eq "prop-link"))
            {
                $self->{curInfo}->{released} = $attr->{title};
                $self->{curInfo}->{released} =~ s/([^0-9])//g;
            }
            elsif (($tagname eq "p") && ($attr->{class} eq "sentences"))
            {
                $self->{isInfo} = 1;
            }
            elsif ($tagname eq "h6")
            {
                $self->{isInfo2} = 1;
            }
            elsif (($tagname eq "a") && ($self->{insideDesigner} eq "1"))
            {
                # Append text (and trailing ,) to existing designer field
                $self->{curInfo}->{designedby} .= $attr->{title} .", ";
            }
            elsif (($tagname eq "a") && ($self->{insideIllustrator} eq "1"))
            {
                # Append text (and trailing ,) to existing designer field
                $self->{curInfo}->{illustratedby} .= $attr->{title} .", ";
            }
            elsif (($tagname eq "a") && ($self->{insidePublishers} eq "1"))
            {
                # Append text (and trailing ,) to existing designer field
                $self->{curInfo}->{publishedby} .= $attr->{title} .", ";
            }
            elsif (($tagname eq "div") && ($self->{insidePlayers} eq 1))
            {
                $self->{insidePlayers} = 2;
            }
            elsif (($tagname eq "div") && ($self->{insideAges} eq 1))
            {
                $self->{insideAges} = 2;
            }
            elsif (($tagname eq "div") && ($self->{insidePlayingTime} eq 1))
            {
                $self->{insidePlayingTime} = 2;
            }
            elsif (($tagname eq "div") && ($self->{isInfo}))
            {
                $self->{isInfo} = 0;
            }
            elsif (($tagname eq "h3") && ($attr->{itemprop} eq "description"))
            {
                $self->{insideDescription} = 1;
            }
            elsif (($tagname eq "a") && ($attr->{title} eq "Extensions sur ce jeu"))
            {

                my $found = 0;
		        my $page = $self->loadPage($attr->{href});
                $page = substr($page, 0,index($page,"<section class=\"suggestions"));
				$page =~ s/&#039;/'/g;
				$page =~ s/&amp;/&/g;
		        while ($page =~ /<div\s+class="game\s+level-1">(.*?)<\/div>/s )
		        {
                    $found = index($page,"<div class=\"game level-1\">");
    		        $page = substr($page, $found +length('<div class="game level-1">'),length($page)- $found -length('<div class="game level-1">'));
     		        $page =~ /<a href="(.*?)\s+title="(.*?)">/s ;
                    $self->{curInfo}->{expandedby} .= $2 .',';
		        }
            }
            elsif (($tagname eq "a") && ($attr->{title} eq "M�dias pour ce jeu"))
            {

                my $found = 0;
                my $found1 = 0;
                my $picture = "";
		        my $page = $self->loadPage($attr->{href});
                $found = index($page,"<section class=\"photos\" id=\"media-library\">");
  		        $page = substr($page, $found +length('<section class="photos" id="media-library">'),length($page)- $found -length('<section class="photos" id="media-library">'));
				$page =~ s/&#039;/'/g;
				$page =~ s/&amp;/&/g;
		        while ($page =~ /<a href="(.*?)"\s+title="(.*?)" data-reveal-id="mediaModal" data-reveal-ajax="true">/s )
		        {
                    $picture = $self->loadPage($1);
                    $found1 = index($picture,"src=\"");
    		        $picture = substr($picture, $found1 +length('src="'),length($picture)- $found1 -length('src="'));
                    $picture = substr($picture, 0,index($picture,"\""));
                    if ($self->{curInfo}->{photo1} eq "" )
	     	        {
	     	        	$self->{curInfo}->{photo1} = $picture;
                        $self->{curInfo}->{photo1} =~ s/https/http/g;
     		        }
                    elsif ($self->{curInfo}->{photo2} eq "" )
	     	        {
	     	        	$self->{curInfo}->{photo2} = $picture;
                        $self->{curInfo}->{photo2} =~ s/https/http/g;
     		        }
                    elsif ($self->{curInfo}->{photo3} eq "" )
	     	        {
	     	        	$self->{curInfo}->{photo3} = $picture;
                        $self->{curInfo}->{photo3} =~ s/https/http/g;
     		        }
                    elsif ($self->{curInfo}->{photo4} eq "" )
	     	        {
	     	        	$self->{curInfo}->{photo4} = $picture;
                        $self->{curInfo}->{photo4} =~ s/https/http/g;
     		        }
                    $found = index($page,"data-reveal-id=\"mediaModal\" data-reveal-ajax=\"true\">");
    		        $page = substr($page, $found +length('data-reveal-id="mediaModal" data-reveal-ajax="true">'),length($page)- $found -length('data-reveal-id="mediaModal" data-reveal-ajax="true">'));
		        }
            }
        }
    }

    sub end
    {
        my ($self, $tagname) = @_;	
        $self->{inside}->{$tagname}--;
        if ($tagname eq "p")
        {
            # Use regex to strip final , off end of line
            $self->{curInfo}->{designedby} =~ s/(, )$//;
            $self->{curInfo}->{illustratedby} =~ s/(, )$//;
            $self->{curInfo}->{publishedby} =~ s/(, )$//;
            $self->{insideDesigner} = 0;
            $self->{insideIllustrator} = 0;
            $self->{insidePublishers} = 0;
        }
    }

    sub text
    {
        my ($self, $origtext) = @_;

        return if (length($origtext) < 2);
        
        $origtext =~ s/&#34;/"/g;
        $origtext =~ s/&#179;/3/g;
        $origtext =~ s/\n//g;
        $origtext =~ s/^\s{2,//;
        #French accents substitution
        $origtext =~ s/&agrave;/�/;
        $origtext =~ s/&eacute;/�/;
        
        return if ($self->{parsingEnded});
        
        if ($self->{parsingList})
        {
    
            # Enleve les blancs en debut de chaine
            $origtext =~ s/^\s+//;
            # Enleve les blancs en fin de chaine
            $origtext =~ s/\s+$//;

            if ($self->{isDate})
            {
               $self->{itemsList}[$self->{itemIdx}]->{released} = $origtext;
               $self->{isDate} = 0;
            }
        }
        else
        {
            # Enleve les blancs en debut de chaine
            $origtext =~ s/^\s+//;
            # Enleve les blancs en fin de chaine
            $origtext =~ s/\s+$//;

            # fetching information from page 
            if (($self->{isInfo} eq "1" ) && !($origtext eq "et") && !($origtext eq ","))
            {
               if ($origtext =~ /Par/)
               {
                  $self->{insideDesigner} = 1;
                  $self->{insideIllustrator} = 0;
                  $self->{insidePublishers} = 0;
               }
               elsif ($origtext =~ /illustr/)
               {
                  $self->{insideDesigner} = 0;
                  $self->{insideIllustrator} = 1;
                  $self->{insidePublishers} = 0;
               }
               elsif ($origtext =~ /dit/)
               {
                  $self->{insideDesigner} = 0;
                  $self->{insideIllustrator} = 0;
                  $self->{insidePublishers} = 1;
               }
               elsif ($origtext =~ /distribu/)
               {
                  $self->{insideDesigner} = 0;
                  $self->{insideIllustrator} = 0;
                  $self->{insidePublishers} = 0;
               }
            }
            elsif ($self->{insidePlayers} eq "2")
            {
                $self->{curInfo}->{players} = $origtext;
                $self->{insidePlayers} = 0;
                $self->{isInfo2} = 0;
            }
            elsif ($self->{insideAges} eq "2")
            {
                $self->{curInfo}->{suggestedage} = $origtext;
                $self->{insideAges} = 0;
                $self->{isInfo2} = 0;
            }
            elsif ($self->{insidePlayingTime} eq "2")
            {
                $self->{curInfo}->{playingtime} = $origtext;
                $self->{insidePlayingTime} = 0;
                $self->{isInfo2} = 0;
            }            
            elsif ($self->{isInfo2} eq "1" )
            {
               if ($origtext =~ /joueur/)
               {
                  $self->{insidePlayers} = 1;
                  $self->{insideAges} = 0;
                  $self->{insidePlayingTime} = 0;
               }
               elsif ($origtext =~ /�ge/)
               {
                  $self->{insidePlayers} = 0;
                  $self->{insideAges} = 1;
                  $self->{insidePlayingTime} = 0;
               }
               elsif ($origtext =~ /dur/)
               {
                  $self->{insidePlayers} = 0;
                  $self->{insideAges} = 0;
                  $self->{insidePlayingTime} = 1;
               }
            }
            elsif ($self->{insideDescription} eq "1")
            {
                $self->{curInfo}->{description} = $origtext;
                $self->{insideDescription} = 0;
            }            
        }
    }

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();
        bless ($self, $class);

        $self->{hasField} = {
            name => 1,
            released => 1,
        };

        $self->{isBoardgame} = 0;
        $self->{isDate} = 0;
        $self->{isInfo} = 0;
        $self->{isInfo2} = 0;
        $self->{insideDesigner} = 0;
        $self->{insideIllustrator} = 0;
        $self->{insidePublishers} = 0;
        $self->{insidePlayers} = 0;
        $self->{insideAges} = 0;
        $self->{insidePlayingTime} = 0;
        $self->{insideDescription} = 0;
        $self->{curName} = undef;
        $self->{curUrl} = undef;

        return $self;
    }

    sub preProcess
    {
        my ($self, $html) = @_;
        
        $self->{parsingEnded} = 0;
        
        $html =~ s/"&#34;/'"/g;
        $html =~ s/&#34;"/"'/g;
        $html =~ s|</a></b><br>|</a><br>|;

        $html =~ s|\x{92}|'|gi;
        $html =~ s|&#146;|'|gi;
        $html =~ s|&#149;|*|gi;
        $html =~ s|&#133;|...|gi;
        $html =~ s|\x{85}|...|gi;
        $html =~ s|\x{8C}|OE|gi;
        $html =~ s|\x{9C}|oe|gi;

        return $html;
    }

    sub getSearchUrl
    {
    	my ($self, $word) = @_;
	  
        # Url returned below is the for the search page, where $word is replaced by the search  
	    return "https://www.trictrac.net/recherche?type=boardgames&page=1&limit=32&display=default&search=$word";
    }
    
    sub getItemUrl
    {
        my ($self, $url) = @_;
		
        return $url if (($url =~ /^http:/) || ($url =~ /^https:/));
        if ($url =~ /^\//)
        {
            return "http://trictrac.net".$url;
        }
        else
        {
            return "http://trictrac.net/".$url;
        }
    }

    sub getName
    {
        return "Tric Trac";
    }
    
    sub getAuthor
    {
        return 'Florent';
    }
    
    sub getLang
    {
        return 'FR';
    }
    
    
}

1;
